import logging

logger = logging.getLogger(__name__)  # recommended to use __name__

# see: https://docs.python.org/3.7/library/logging.html#logrecord-attributes
# for format options and explanations
logging.basicConfig(filename='Application.log', level=logging.DEBUG,
                    format='%(asctime)s:%(name)s:%(levelname)s:%(message)s')

logger.debug('log debug message')
logger.info('log info message')
logger.warning('log warning message')
logger.error('log error message')
logger.critical('log critical message')