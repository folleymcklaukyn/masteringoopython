class PythonClass(object):

    def __init__(self): # initializer - automatically runs when class gets created
        print(self)     # print instance (self)

# Create instance(s) of class

first_instance = PythonClass()
second_instance = PythonClass()

# Result - two distinc objects located at different memory addresses:
# Example:
# <__main__.PythonClass object at 0x0000025494EA7348>
# <__main__.PythonClass object at 0x0000025494EA7D88>

first_instance_copy = first_instance

print(first_instance == first_instance_copy)
print(first_instance is first_instance_copy)

print(first_instance)
print(first_instance_copy)